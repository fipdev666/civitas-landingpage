# CIVITAS LANDINGPAGE

## 📓 Conteúdo do projeto

- JavaScript - DOM
- HTTP protocol
- CRUD - HTTP methods
- Fetch API

<hr><br>

## 🛠 Tecnologias e ferramentas

- [JavaScript](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript)
- [Fetch API](https://developer.mozilla.org/pt-BR/docs/Web/API/Fetch_API)
- [JSON Server](https://www.npmjs.com/package/json-server)

<hr><br>

## 💻 Rodando o projeto na máquina

<br>

#### ➡️ Clone esse repositório

```
git clone git@gitlab.com:fipdev666/civitas-landingpage.git
```

#### ➡️ Entre na pasta do projeto

```
cd civitas-landingpage
```

#### ➡️ Abra o terminal (pode ser pelo VSCode) e execute o comando para instalar as dependências

```
npm install
```

#### ➡️ Execute o comando para iniciar a API

```
json-server --watch db.json

           ou

npx json-server --watch db.json

```

#### ➡️ Abra o arquivo `index.html` para iniciar a aplicação

<br>

#### ➡️ Para parar de servir a API, basta executar o comando `CTRL + C` no terminal

<hr>

<div align="center">
Copyright © 2022<br> 
  💥😈💥 Created by <b>fipdev666</b> 💥😈💥
</div>
