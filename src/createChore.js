/* Pegar elementos DOM*/

const inputName = document.getElementById("input-name");
const inputEmail = document.getElementById("input-email");
const inputPassword = document.getElementById("input-password");
const inputDate = document.getElementById("input-date");
const signUpBtn = document.getElementById("sign-up-btn");
const errSpan = document.getElementById("error");
import {postChores} from './postChore.js';


signUpBtn.addEventListener('click', async (event) => {
    event.preventDefault();
    const nome = inputName.value;
    const email = inputEmail.value;
    const senha = inputPassword.value;
    const data = inputDate.value;

    if (nome==="" || senha==="" || email==="" || data===""){
      errSpan.style.display="block"; 
    } else{
        errSpan.style.display="none";
       await postChores(nome, email, senha, data);
    }
});

